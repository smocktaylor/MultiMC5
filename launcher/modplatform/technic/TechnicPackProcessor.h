/* Copyright 2020-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QString>
#include "settings/SettingsObject.h"

namespace Technic
{
    // not exporting it, only used in SingleZipPackInstallTask, InstanceImportTask and SolderPackInstallTask
    class TechnicPackProcessor : public QObject
    {
        Q_OBJECT

    signals:
        void succeeded();
        void failed(QString reason);

    public:
        void run(SettingsObjectPtr globalSettings, const QString &instName, const QString &instIcon, const QString &stagingPath, const QString &minecraftVersion=QString(), const bool isSolder = false);
    };
}
