/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <InstanceTask.h>
#include <net/NetJob.h>
#include <tasks/Task.h>

#include <QUrl>

namespace Technic
{
    class SolderPackInstallTask : public InstanceTask
    {
        Q_OBJECT
    public:
        explicit SolderPackInstallTask(shared_qobject_ptr<QNetworkAccessManager> network, const QUrl &sourceUrl, const QString &minecraftVersion);

        bool canAbort() const override { return true; }
        bool abort() override;

    protected:
        //! Entry point for tasks.
        virtual void executeTask() override;

    private slots:
        void versionSucceeded();
        void fileListSucceeded();
        void downloadSucceeded();
        void downloadFailed(QString reason);
        void downloadProgressChanged(qint64 current, qint64 total);
        void extractFinished();
        void extractAborted();

    private:
        bool m_abortable = false;

        shared_qobject_ptr<QNetworkAccessManager> m_network;

        NetJob::Ptr m_filesNetJob;
        QUrl m_sourceUrl;
        QString m_minecraftVersion;
        QByteArray m_response;
        QTemporaryDir m_outputDir;
        int m_modCount;
        QFuture<bool> m_extractFuture;
        QFutureWatcher<bool> m_extractFutureWatcher;
    };
}
