/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#include "LaunchStep.h"
#include "LaunchTask.h"

void LaunchStep::bind(LaunchTask *parent)
{
    m_parent = parent;
    connect(this, &LaunchStep::readyForLaunch, parent, &LaunchTask::onReadyForLaunch);
    connect(this, &LaunchStep::logLine, parent, &LaunchTask::onLogLine);
    connect(this, &LaunchStep::logLines, parent, &LaunchTask::onLogLines);
    connect(this, &LaunchStep::finished, parent, &LaunchTask::onStepFinished);
    connect(this, &LaunchStep::progressReportingRequest, parent, &LaunchTask::onProgressReportingRequested);
}
