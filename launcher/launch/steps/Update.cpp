/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */


#include "Update.h"
#include <launch/LaunchTask.h>

void Update::executeTask()
{
    if(m_aborted)
    {
        emitFailed(tr("Task aborted."));
        return;
    }
    m_updateTask.reset(m_parent->instance()->createUpdateTask(m_mode));
    if(m_updateTask)
    {
        connect(m_updateTask.get(), SIGNAL(finished()), this, SLOT(updateFinished()));
        connect(m_updateTask.get(), &Task::progress, this, &Task::setProgress);
        connect(m_updateTask.get(), &Task::status, this, &Task::setStatus);
        emit progressReportingRequest();
        return;
    }
    emitSucceeded();
}

void Update::proceed()
{
    m_updateTask->start();
}

void Update::updateFinished()
{
    if(m_updateTask->wasSuccessful())
    {
        m_updateTask.reset();
        emitSucceeded();
    }
    else
    {
        QString reason = tr("Instance update failed because: %1\n\n").arg(m_updateTask->failReason());
        m_updateTask.reset();
        emit logLine(reason, MessageLevel::Fatal);
        emitFailed(reason);
    }
}

bool Update::canAbort() const
{
    if(m_updateTask)
    {
        return m_updateTask->canAbort();
    }
    return true;
}


bool Update::abort()
{
    m_aborted = true;
    if(m_updateTask)
    {
        if(m_updateTask->canAbort())
        {
            return m_updateTask->abort();
        }
    }
    return true;
}
