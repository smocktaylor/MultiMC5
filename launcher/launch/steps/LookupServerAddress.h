/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <launch/LaunchStep.h>
#include <QObjectPtr.h>
#include <QDnsLookup>

#include "minecraft/launch/MinecraftServerTarget.h"

class LookupServerAddress: public LaunchStep {
Q_OBJECT
public:
    explicit LookupServerAddress(LaunchTask *parent);
    virtual ~LookupServerAddress() {};

    virtual void executeTask();
    virtual bool abort();
    virtual bool canAbort() const
    {
        return true;
    }

    void setLookupAddress(const QString &lookupAddress);
    void setOutputAddressPtr(MinecraftServerTargetPtr output);

private slots:
    void on_dnsLookupFinished();

private:
    void resolve(const QString &address, quint16 port);

    QDnsLookup *m_dnsLookup;
    QString m_lookupAddress;
    MinecraftServerTargetPtr m_output;
};
