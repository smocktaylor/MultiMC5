/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <launch/LaunchStep.h>
#include <QObjectPtr.h>
#include <LoggedProcess.h>
#include <java/JavaChecker.h>
#include <net/Mode.h>

// FIXME: stupid. should be defined by the instance type? or even completely abstracted away...
class Update: public LaunchStep
{
    Q_OBJECT
public:
    explicit Update(LaunchTask *parent, Net::Mode mode):LaunchStep(parent), m_mode(mode) {};
    virtual ~Update() {};

    void executeTask() override;
    bool canAbort() const override;
    void proceed() override;
public slots:
    bool abort() override;

private slots:
    void updateFinished();

private:
    Task::Ptr m_updateTask;
    bool m_aborted = false;
    Net::Mode m_mode = Net::Mode::Offline;
};
