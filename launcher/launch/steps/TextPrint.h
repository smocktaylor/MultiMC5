/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <launch/LaunchStep.h>
#include <LoggedProcess.h>
#include <java/JavaChecker.h>

/*
 * FIXME: maybe do not export
 */

class TextPrint: public LaunchStep
{
    Q_OBJECT
public:
    explicit TextPrint(LaunchTask *parent, const QStringList &lines, MessageLevel::Enum level);
    explicit TextPrint(LaunchTask *parent, const QString &line, MessageLevel::Enum level);
    virtual ~TextPrint(){};

    virtual void executeTask();
    virtual bool canAbort() const;
    virtual bool abort();

private:
    QStringList m_lines;
    MessageLevel::Enum m_level;
};
