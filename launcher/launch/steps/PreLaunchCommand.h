/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include "launch/LaunchStep.h"
#include "LoggedProcess.h"

class PreLaunchCommand: public LaunchStep
{
    Q_OBJECT
public:
    explicit PreLaunchCommand(LaunchTask *parent);
    virtual ~PreLaunchCommand() {};

    virtual void executeTask();
    virtual bool abort();
    virtual bool canAbort() const
    {
        return true;
    }
    void setWorkingDirectory(const QString &wd);
private slots:
    void on_state(LoggedProcess::State state);

private:
    LoggedProcess m_process;
    QString m_command;
};
