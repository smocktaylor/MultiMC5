/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <launch/LaunchStep.h>
#include <LoggedProcess.h>
#include <java/JavaChecker.h>

class CheckJava: public LaunchStep
{
    Q_OBJECT
public:
    explicit CheckJava(LaunchTask *parent) :LaunchStep(parent){};
    virtual ~CheckJava() {};

    virtual void executeTask();
    virtual bool canAbort() const
    {
        return false;
    }
private slots:
    void checkJavaFinished(JavaCheckResult result);

private:
    void printJavaInfo(const QString & version, const QString & architecture, const QString & vendor);
    void printSystemInfo(bool javaIsKnown, bool javaIs64bit);

private:
    QString m_javaPath;
    qlonglong m_javaUnixTime;
    JavaCheckerPtr m_JavaChecker;
};
