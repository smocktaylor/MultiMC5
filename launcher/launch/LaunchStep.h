/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include "tasks/Task.h"
#include "MessageLevel.h"

#include <QStringList>

class LaunchTask;
class LaunchStep: public Task
{
    Q_OBJECT
public: /* methods */
    explicit LaunchStep(LaunchTask *parent):Task(nullptr), m_parent(parent)
    {
        bind(parent);
    };
    virtual ~LaunchStep() {};

private: /* methods */
    void bind(LaunchTask *parent);

signals:
    void logLines(QStringList lines, MessageLevel::Enum level);
    void logLine(QString line, MessageLevel::Enum level);
    void readyForLaunch();
    void progressReportingRequest();

public slots:
    virtual void proceed() {};
    // called in the opposite order than the Task launch(), used to clean up or otherwise undo things after the launch ends
    virtual void finalize() {};

protected: /* data */
    LaunchTask *m_parent;
};
