/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#include "SkinUtils.h"
#include "net/HttpMetaCache.h"
#include "Application.h"

#include <QFile>
#include <QPainter>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

namespace SkinUtils
{
/*
 * Given a username, return a pixmap of the cached skin (if it exists), QPixmap() otherwise
 */
QPixmap getFaceFromCache(QString username, int height, int width)
{
    QFile fskin(APPLICATION->metacache()->resolveEntry("skins", username + ".png")->getFullPath());

    if (fskin.exists())
    {
        QPixmap skinTexture(fskin.fileName());
        if(!skinTexture.isNull())
        {
            QPixmap skin = QPixmap(8, 8);
            QPainter painter(&skin);
            painter.drawPixmap(0, 0, skinTexture.copy(8, 8, 8, 8));
            painter.drawPixmap(0, 0, skinTexture.copy(40, 8, 8, 8));
            return skin.scaled(height, width, Qt::KeepAspectRatio);
        }
    }

    return QPixmap();
}
}
