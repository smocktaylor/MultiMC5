/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QtNetwork>
#include "JavaChecker.h"
#include "tasks/Task.h"

class JavaCheckerJob;
typedef shared_qobject_ptr<JavaCheckerJob> JavaCheckerJobPtr;

// FIXME: this just seems horribly redundant
class JavaCheckerJob : public Task
{
    Q_OBJECT
public:
    explicit JavaCheckerJob(QString job_name) : Task(), m_job_name(job_name) {};
    virtual ~JavaCheckerJob() {};

    bool addJavaCheckerAction(JavaCheckerPtr base)
    {
        javacheckers.append(base);
        // if this is already running, the action needs to be started right away!
        if (isRunning())
        {
            setProgress(num_finished, javacheckers.size());
            connect(base.get(), &JavaChecker::checkFinished, this, &JavaCheckerJob::partFinished);
            base->performCheck();
        }
        return true;
    }
    QList<JavaCheckResult> getResults()
    {
        return javaresults;
    }

private slots:
    void partFinished(JavaCheckResult result);

protected:
    virtual void executeTask() override;

private:
    QString m_job_name;
    QList<JavaCheckerPtr> javacheckers;
    QList<JavaCheckResult> javaresults;
    int num_finished = 0;
};
