/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QObject>
#include <QAbstractListModel>

#include "BaseVersionList.h"
#include "tasks/Task.h"

#include "JavaCheckerJob.h"
#include "JavaInstall.h"

#include "QObjectPtr.h"

class JavaListLoadTask;

class JavaInstallList : public BaseVersionList
{
    Q_OBJECT
    enum class Status
    {
        NotDone,
        InProgress,
        Done
    };
public:
    explicit JavaInstallList(QObject *parent = 0);

    Task::Ptr getLoadTask() override;
    bool isLoaded() override;
    const BaseVersionPtr at(int i) const override;
    int count() const override;
    void sortVersions() override;

    QVariant data(const QModelIndex &index, int role) const override;
    RoleList providesRoles() const override;

public slots:
    void updateListData(QList<BaseVersionPtr> versions) override;

protected:
    void load();
    Task::Ptr getCurrentTask();

protected:
    Status m_status = Status::NotDone;
    shared_qobject_ptr<JavaListLoadTask> m_loadTask;
    QList<BaseVersionPtr> m_vlist;
};

class JavaListLoadTask : public Task
{
    Q_OBJECT

public:
    explicit JavaListLoadTask(JavaInstallList *vlist);
    virtual ~JavaListLoadTask();

    void executeTask() override;
public slots:
    void javaCheckerFinished();

protected:
    shared_qobject_ptr<JavaCheckerJob> m_job;
    JavaInstallList *m_list;
    JavaInstall *m_currentRecommended;
};
