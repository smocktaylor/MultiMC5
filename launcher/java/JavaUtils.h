/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QStringList>

#include "JavaChecker.h"
#include "JavaInstallList.h"

#ifdef Q_OS_WIN
#include <windows.h>
#endif

QProcessEnvironment CleanEnviroment();

class JavaUtils : public QObject
{
    Q_OBJECT
public:
    JavaUtils();

    JavaInstallPtr MakeJavaPtr(QString path, QString id = "unknown", QString arch = "unknown");
    QList<QString> FindJavaPaths();
    JavaInstallPtr GetDefaultJava();

#ifdef Q_OS_WIN
    QList<JavaInstallPtr> FindJavaFromRegistryKey(DWORD keyType, QString keyName, QString keyJavaDir, QString subkeySuffix = "");
#endif
};
