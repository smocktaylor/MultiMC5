/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#include "JavaCheckerJob.h"

#include <QDebug>

void JavaCheckerJob::partFinished(JavaCheckResult result)
{
    num_finished++;
    qDebug() << m_job_name.toLocal8Bit() << "progress:" << num_finished << "/"
                << javacheckers.size();
    setProgress(num_finished, javacheckers.size());

    javaresults.replace(result.id, result);

    if (num_finished == javacheckers.size())
    {
        emitSucceeded();
    }
}

void JavaCheckerJob::executeTask()
{
    qDebug() << m_job_name.toLocal8Bit() << " started.";
    for (auto iter : javacheckers)
    {
        javaresults.append(JavaCheckResult());
        connect(iter.get(), SIGNAL(checkFinished(JavaCheckResult)), SLOT(partFinished(JavaCheckResult)));
        iter->performCheck();
    }
}
