/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QAbstractListModel>
#include <memory>

struct Language;

class TranslationsModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit TranslationsModel(QString path, QObject *parent = 0);
    virtual ~TranslationsModel();

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex & parent) const override;

    bool selectLanguage(QString key);
    void updateLanguage(QString key);
    QModelIndex selectedIndex();
    QString selectedLanguage();

    void downloadIndex();

private:
    Language *findLanguage(const QString & key);
    void reloadLocalFiles();
    void downloadTranslation(QString key);
    void downloadNext();

    // hide copy constructor
    TranslationsModel(const TranslationsModel &) = delete;
    // hide assign op
    TranslationsModel &operator=(const TranslationsModel &) = delete;

private slots:
    void indexReceived();
    void indexFailed(QString reason);
    void dlFailed(QString reason);
    void dlGood();
    void translationDirChanged(const QString &path);


private: /* data */
    struct Private;
    std::unique_ptr<Private> d;
};
