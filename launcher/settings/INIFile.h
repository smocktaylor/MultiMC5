/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QString>
#include <QVariant>
#include <QIODevice>

// Sectionless INI parser (for instance config files)
class INIFile : public QMap<QString, QVariant>
{
public:
    explicit INIFile();

    bool loadFile(QByteArray file);
    bool loadFile(QString fileName);
    bool saveFile(QString fileName);

    QVariant get(QString key, QVariant def) const;
    void set(QString key, QVariant val);
    static QString unescape(QString orig);
    static QString escape(QString orig);
};
