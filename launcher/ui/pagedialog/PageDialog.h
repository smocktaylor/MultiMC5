/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QDialog>
#include "ui/pages/BasePageProvider.h"

class PageContainer;
class PageDialog : public QDialog
{
    Q_OBJECT
public:
    explicit PageDialog(BasePageProvider *pageProvider, QString defaultId = QString(), QWidget *parent = 0);
    virtual ~PageDialog() {}

private
slots:
    virtual void closeEvent(QCloseEvent *event);

private:
    PageContainer * m_container;
};
