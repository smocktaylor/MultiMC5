/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once
#include <QTreeView>

class ModListView: public QTreeView
{
    Q_OBJECT
public:
    explicit ModListView ( QWidget* parent = 0 );
    virtual void setModel ( QAbstractItemModel* model );
};
