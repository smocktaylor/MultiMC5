/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QWidget>
#include <QModelIndex>

#include "ui/pages/BasePageProvider.h"
#include "ui/pages/BasePageContainer.h"

class QLayout;
class IconLabel;
class QSortFilterProxyModel;
class PageModel;
class QLabel;
class QListView;
class QLineEdit;
class QStackedLayout;
class QGridLayout;

class PageContainer : public QWidget, public BasePageContainer
{
    Q_OBJECT
public:
    explicit PageContainer(BasePageProvider *pageProvider, QString defaultId = QString(),
                        QWidget *parent = 0);
    virtual ~PageContainer() {}

    void addButtons(QWidget * buttons);
    void addButtons(QLayout * buttons);
    /*
     * Save any unsaved state and prepare to be closed.
     * @return true if everything can be saved, false if there is something that requires attention
     */
    bool prepareToClose();
    bool saveAll();

    /* request close - used by individual pages */
    bool requestClose() override
    {
        if(m_container)
        {
            return m_container->requestClose();
        }
        return false;
    }

    virtual bool selectPage(QString pageId) override;

    void refreshContainer() override;
    virtual void setParentContainer(BasePageContainer * container)
    {
        m_container = container;
    };

private:
    void createUI();

public slots:
    void help();

private slots:
    void currentChanged(const QModelIndex &current);
    void showPage(int row);

private:
    BasePageContainer * m_container = nullptr;
    BasePage * m_currentPage = 0;
    QSortFilterProxyModel *m_proxyModel;
    PageModel *m_model;
    QStackedLayout *m_pageStack;
    QListView *m_pageList;
    QLabel *m_header;
    IconLabel *m_iconHeader;
    QGridLayout *m_layout;
};
