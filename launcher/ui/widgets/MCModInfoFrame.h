/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QFrame>
#include "minecraft/mod/Mod.h"

namespace Ui
{
class MCModInfoFrame;
}

class MCModInfoFrame : public QFrame
{
    Q_OBJECT

public:
    explicit MCModInfoFrame(QWidget *parent = 0);
    ~MCModInfoFrame();

    void setModText(QString text);
    void setModDescription(QString text);

    void updateWithMod(Mod &m);
    void clear();

public slots:
    void modDescEllipsisHandler(const QString& link );
    void boxClosed(int result);

private:
    void updateHiddenState();

private:
    Ui::MCModInfoFrame *ui;
    QString desc;
    class QMessageBox * currentBox = nullptr;
};

