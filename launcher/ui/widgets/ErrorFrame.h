/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QFrame>

namespace Ui
{
class ErrorFrame;
}

class ErrorFrame : public QFrame
{
    Q_OBJECT

public:
    explicit ErrorFrame(QWidget *parent = 0);
    ~ErrorFrame();

    void setTitle(QString text);
    void setDescription(QString text);

    void clear();

public slots:
    void ellipsisHandler(const QString& link );
    void boxClosed(int result);

private:
    void updateHiddenState();

private:
    Ui::ErrorFrame *ui;
    QString desc;
    class QMessageBox * currentBox = nullptr;
};
