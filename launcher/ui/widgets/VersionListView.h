/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once
#include <QTreeView>

class VersionListView : public QTreeView
{
    Q_OBJECT
public:

    explicit VersionListView(QWidget *parent = 0);
    virtual void paintEvent(QPaintEvent *event) override;
    virtual void setModel(QAbstractItemModel* model) override;

    enum EmptyMode
    {
        Empty,
        String,
        ErrorString
    };

    void setEmptyString(QString emptyString);
    void setEmptyErrorString(QString emptyErrorString);
    void setEmptyMode(EmptyMode mode);

public slots:
    virtual void reset() override;

protected slots:
    virtual void rowsAboutToBeRemoved(const QModelIndex & parent, int start, int end) override;
    virtual void rowsInserted(const QModelIndex &parent, int start, int end) override;

private: /* methods */
    void paintInfoLabel(QPaintEvent *event) const;
    void updateEmptyViewPort();
    QString currentEmptyString() const;

private: /* variables */
    int m_itemCount = 0;
    QString m_emptyString;
    QString m_emptyErrorString;
    EmptyMode m_emptyMode = Empty;
};
