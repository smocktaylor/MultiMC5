/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QWidget>

class QVBoxLayout;
class QTreeView;
class QLabel;

class LanguageSelectionWidget: public QWidget
{
    Q_OBJECT
public:
    explicit LanguageSelectionWidget(QWidget *parent = 0);
    virtual ~LanguageSelectionWidget() { };

    QString getSelectedLanguageKey() const;
    void retranslate();

protected slots:
    void languageRowChanged(const QModelIndex &current, const QModelIndex &previous);

private:
    QVBoxLayout *verticalLayout = nullptr;
    QTreeView *languageView = nullptr;
    QLabel *helpUsLabel = nullptr;
};
