/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QMainWindow>
#include <QSystemTrayIcon>

#include "LaunchController.h"
#include "launch/LaunchTask.h"

#include "ui/pages/BasePageContainer.h"

#include "QObjectPtr.h"

class QPushButton;
class PageContainer;
class InstanceWindow : public QMainWindow, public BasePageContainer
{
    Q_OBJECT

public:
    explicit InstanceWindow(InstancePtr proc, QWidget *parent = 0);
    virtual ~InstanceWindow();

    bool selectPage(QString pageId) override;
    void refreshContainer() override;

    QString instanceId();

    // save all settings and changes (prepare for launch)
    bool saveAll();

    // request closing the window (from a page)
    bool requestClose() override;

signals:
    void isClosing();

private
slots:
    void on_closeButton_clicked();
    void on_btnKillMinecraft_clicked();
    void on_btnLaunchMinecraftOffline_clicked();

    void on_InstanceLaunchTask_changed(shared_qobject_ptr<LaunchTask> proc);
    void on_RunningState_changed(bool running);
    void on_instanceStatusChanged(BaseInstance::Status, BaseInstance::Status newStatus);

protected:
    void closeEvent(QCloseEvent *) override;

private:
    void updateLaunchButtons();

private:
    shared_qobject_ptr<LaunchTask> m_proc;
    InstancePtr m_instance;
    bool m_doNotSave = false;
    PageContainer *m_container = nullptr;
    QPushButton *m_closeButton = nullptr;
    QPushButton *m_killButton = nullptr;
    QPushButton *m_launchOfflineButton = nullptr;
};
