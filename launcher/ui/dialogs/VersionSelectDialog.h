/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QDialog>
#include <QSortFilterProxyModel>


#include "BaseVersionList.h"

class QVBoxLayout;
class QHBoxLayout;
class QDialogButtonBox;
class VersionSelectWidget;
class QPushButton;

namespace Ui
{
class VersionSelectDialog;
}

class VersionProxyModel;

class VersionSelectDialog : public QDialog
{
    Q_OBJECT

public:
    explicit VersionSelectDialog(BaseVersionList *vlist, QString title, QWidget *parent = 0, bool cancelable = true);
    virtual ~VersionSelectDialog() {};

    int exec() override;

    BaseVersionPtr selectedVersion() const;

    void setCurrentVersion(const QString & version);
    void setFuzzyFilter(BaseVersionList::ModelRoles role, QString filter);
    void setExactFilter(BaseVersionList::ModelRoles role, QString filter);
    void setEmptyString(QString emptyString);
    void setEmptyErrorString(QString emptyErrorString);
    void setResizeOn(int column);

private slots:
    void on_refreshButton_clicked();

private:
    void retranslate();
    void selectRecommended();

private:
    QString m_currentVersion;
    VersionSelectWidget *m_versionWidget = nullptr;
    QVBoxLayout *m_verticalLayout = nullptr;
    QHBoxLayout *m_horizontalLayout = nullptr;
    QPushButton *m_refreshButton = nullptr;
    QDialogButtonBox *m_buttonBox = nullptr;

    BaseVersionList *m_vlist = nullptr;

    VersionProxyModel *m_proxyModel = nullptr;

    int resizeOnColumn = -1;

    Task * loadTask = nullptr;
};
