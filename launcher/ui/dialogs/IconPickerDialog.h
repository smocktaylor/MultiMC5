/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once
#include <QDialog>
#include <QItemSelection>

namespace Ui
{
class IconPickerDialog;
}

class IconPickerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit IconPickerDialog(QWidget *parent = 0);
    ~IconPickerDialog();
    int execWithSelection(QString selection);
    QString selectedIconKey;

protected:
    virtual bool eventFilter(QObject *, QEvent *);

private:
    Ui::IconPickerDialog *ui;

private
slots:
    void selectionChanged(QItemSelection, QItemSelection);
    void activated(QModelIndex);
    void delayed_scroll(QModelIndex);
    void addNewIcon();
    void removeSelectedIcon();
    void openFolder();
};
