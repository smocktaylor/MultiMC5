/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QDialog>

namespace Ui
{
class EditAccountDialog;
}

class EditAccountDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EditAccountDialog(const QString &text = "", QWidget *parent = 0,
                               int flags = UsernameField | PasswordField);
    ~EditAccountDialog();

    void setUsername(const QString & user) const;
    void setPassword(const QString & pass) const;

    QString username() const;
    QString password() const;

    enum Flags
    {
        NoFlags = 0,

        //! Specifies that the dialog should have a username field.
        UsernameField,

        //! Specifies that the dialog should have a password field.
        PasswordField,
    };

private slots:
  void on_label_linkActivated(const QString &link);

private:
    Ui::EditAccountDialog *ui;
};
