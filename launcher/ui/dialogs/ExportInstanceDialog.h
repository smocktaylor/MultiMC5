/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QDialog>
#include <QModelIndex>
#include <memory>

class BaseInstance;
class PackIgnoreProxy;
typedef std::shared_ptr<BaseInstance> InstancePtr;

namespace Ui
{
class ExportInstanceDialog;
}

class ExportInstanceDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ExportInstanceDialog(InstancePtr instance, QWidget *parent = 0);
    ~ExportInstanceDialog();

    virtual void done(int result);

private:
    bool doExport();
    void loadPackIgnore();
    void savePackIgnore();
    QString ignoreFileName();

private:
    Ui::ExportInstanceDialog *ui;
    InstancePtr m_instance;
    PackIgnoreProxy * proxyModel;

private slots:
    void rowsInserted(QModelIndex parent, int top, int bottom);
};
