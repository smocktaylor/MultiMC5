/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QDialog>

#include <QString>
#include <QStringList>

namespace Ui
{
class NewComponentDialog;
}

class NewComponentDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewComponentDialog(const QString & initialName = QString(), const QString & initialUid = QString(), QWidget *parent = 0);
    virtual ~NewComponentDialog();
    void setBlacklist(QStringList badUids);

    QString name() const;
    QString uid() const;

private slots:
    void updateDialogState();

private:
    Ui::NewComponentDialog *ui;

    QString originalPlaceholderText;
    QStringList uidBlacklist;
};
