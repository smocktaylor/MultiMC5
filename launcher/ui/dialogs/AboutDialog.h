/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QDialog>
#include <net/NetJob.h>

namespace Ui
{
class AboutDialog;
}

class AboutDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AboutDialog(QWidget *parent = 0);
    ~AboutDialog();

public
slots:
    /// Starts loading a list of Patreon patrons.
    void loadPatronList();
    
    /// Slot for when the patron list loads successfully.
    void patronListLoaded();

private:
    Ui::AboutDialog *ui;

    NetJob::Ptr netJob;
    QByteArray dataSink;
};
