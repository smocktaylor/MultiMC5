/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#include "EditAccountDialog.h"
#include "ui_EditAccountDialog.h"
#include <DesktopServices.h>
#include <QUrl>

EditAccountDialog::EditAccountDialog(const QString &text, QWidget *parent, int flags)
    : QDialog(parent), ui(new Ui::EditAccountDialog)
{
    ui->setupUi(this);

    ui->label->setText(text);
    ui->label->setVisible(!text.isEmpty());

    ui->userTextBox->setEnabled(flags & UsernameField);
    ui->passTextBox->setEnabled(flags & PasswordField);
}

EditAccountDialog::~EditAccountDialog()
{
    delete ui;
}

void EditAccountDialog::on_label_linkActivated(const QString &link)
{
    DesktopServices::openUrl(QUrl(link));
}

void EditAccountDialog::setUsername(const QString & user) const
{
    ui->userTextBox->setText(user);
}

QString EditAccountDialog::username() const
{
    return ui->userTextBox->text();
}

void EditAccountDialog::setPassword(const QString & pass) const
{
    ui->passTextBox->setText(pass);
}

QString EditAccountDialog::password() const
{
    return ui->passTextBox->text();
}
