/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QDialog>
#include "net/NetJob.h"

namespace Ui
{
class UpdateDialog;
}

enum UpdateAction
{
    UPDATE_LATER = QDialog::Rejected,
    UPDATE_NOW = QDialog::Accepted,
};

enum ChangelogType
{
    CHANGELOG_MARKDOWN,
    CHANGELOG_COMMITS
};

class UpdateDialog : public QDialog
{
    Q_OBJECT

public:
    explicit UpdateDialog(bool hasUpdate = true, QWidget *parent = 0);
    ~UpdateDialog();

public slots:
    void on_btnUpdateNow_clicked();
    void on_btnUpdateLater_clicked();

    /// Starts loading the changelog
    void loadChangelog();

    /// Slot for when the chengelog loads successfully.
    void changelogLoaded();

    /// Slot for when the chengelog fails to load...
    void changelogFailed(QString reason);

protected:
    void closeEvent(QCloseEvent * ) override;

private:
    Ui::UpdateDialog *ui;
    QByteArray changelogData;
    NetJob::Ptr dljob;
    ChangelogType m_changelogType = CHANGELOG_MARKDOWN;
};
