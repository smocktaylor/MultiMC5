/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QSortFilterProxyModel>
#include <QCollator>

class InstanceProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT

public:
    InstanceProxyModel(QObject *parent = 0);

protected:
    QVariant data(const QModelIndex & index, int role) const override;
    bool lessThan(const QModelIndex &left, const QModelIndex &right) const override;
    bool subSortLessThan(const QModelIndex &left, const QModelIndex &right) const;

private:
    QCollator m_naturalSort;
};
