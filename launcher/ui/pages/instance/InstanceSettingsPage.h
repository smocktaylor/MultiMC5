/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QWidget>

#include "java/JavaChecker.h"
#include "BaseInstance.h"
#include <QObjectPtr.h>
#include "ui/pages/BasePage.h"
#include "JavaCommon.h"
#include "Application.h"

class JavaChecker;
namespace Ui
{
class InstanceSettingsPage;
}

class InstanceSettingsPage : public QWidget, public BasePage
{
    Q_OBJECT

public:
    explicit InstanceSettingsPage(BaseInstance *inst, QWidget *parent = 0);
    virtual ~InstanceSettingsPage();
    virtual QString displayName() const override
    {
        return tr("Settings");
    }
    virtual QIcon icon() const override
    {
        return APPLICATION->getThemedIcon("instance-settings");
    }
    virtual QString id() const override
    {
        return "settings";
    }
    virtual bool apply() override;
    virtual QString helpPage() const override
    {
        return "Instance-settings";
    }
    virtual bool shouldDisplay() const override;

private slots:
    void on_javaDetectBtn_clicked();
    void on_javaTestBtn_clicked();
    void on_javaBrowseBtn_clicked();

    void applySettings();
    void loadSettings();

    void checkerFinished();

    void globalSettingsButtonClicked(bool checked);

private:
    Ui::InstanceSettingsPage *ui;
    BaseInstance *m_instance;
    SettingsObjectPtr m_settings;
    unique_qobject_ptr<JavaCommon::TestCheck> checker;
};
