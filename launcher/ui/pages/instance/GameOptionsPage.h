/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QWidget>
#include <QString>

#include "ui/pages/BasePage.h"
#include <Application.h>

namespace Ui
{
class GameOptionsPage;
}

class GameOptions;
class MinecraftInstance;

class GameOptionsPage : public QWidget, public BasePage
{
    Q_OBJECT

public:
    explicit GameOptionsPage(MinecraftInstance *inst, QWidget *parent = 0);
    virtual ~GameOptionsPage();

    void openedImpl() override;
    void closedImpl() override;

    virtual QString displayName() const override
    {
        return tr("Game Options");
    }
    virtual QIcon icon() const override
    {
        return APPLICATION->getThemedIcon("settings");
    }
    virtual QString id() const override
    {
        return "gameoptions";
    }
    virtual QString helpPage() const override
    {
        return "Game-Options-management";
    }

private: // data
    Ui::GameOptionsPage *ui = nullptr;
    std::shared_ptr<GameOptions> m_model;
};
