/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QMainWindow>

#include "ui/pages/BasePage.h"
#include <Application.h>

class QFileSystemModel;
class QIdentityProxyModel;
namespace Ui
{
class ScreenshotsPage;
}

struct ScreenShot;
class ScreenshotList;
class ImgurAlbumCreation;

class ScreenshotsPage : public QMainWindow, public BasePage
{
    Q_OBJECT

public:
    explicit ScreenshotsPage(QString path, QWidget *parent = 0);
    virtual ~ScreenshotsPage();

    virtual void openedImpl() override;

    enum
    {
        NothingDone = 0x42
    };

    virtual bool eventFilter(QObject *, QEvent *) override;
    virtual QString displayName() const override
    {
        return tr("Screenshots");
    }
    virtual QIcon icon() const override
    {
        return APPLICATION->getThemedIcon("screenshots");
    }
    virtual QString id() const override
    {
        return "screenshots";
    }
    virtual QString helpPage() const override
    {
        return "Screenshots-management";
    }
    virtual bool apply() override
    {
        return !m_uploadActive;
    }

protected:
    QMenu * createPopupMenu() override;

private slots:
    void on_actionUpload_triggered();
    void on_actionCopy_Image_triggered();
    void on_actionCopy_File_s_triggered();
    void on_actionDelete_triggered();
    void on_actionRename_triggered();
    void on_actionView_Folder_triggered();
    void onItemActivated(QModelIndex);
    void ShowContextMenu(const QPoint &pos);

private:
    Ui::ScreenshotsPage *ui;
    std::shared_ptr<QFileSystemModel> m_model;
    std::shared_ptr<QIdentityProxyModel> m_filterModel;
    QString m_folder;
    bool m_valid = false;
    bool m_uploadActive = false;
};
