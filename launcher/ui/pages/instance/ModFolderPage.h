/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QMainWindow>

#include "minecraft/MinecraftInstance.h"
#include "ui/pages/BasePage.h"

#include <Application.h>

class ModFolderModel;
namespace Ui
{
class ModFolderPage;
}

class ModFolderPage : public QMainWindow, public BasePage
{
    Q_OBJECT

public:
    explicit ModFolderPage(
        BaseInstance *inst,
        std::shared_ptr<ModFolderModel> mods,
        QString id,
        QString iconName,
        QString displayName,
        QString helpPage = "",
        QWidget *parent = 0
    );
    virtual ~ModFolderPage();

    void setFilter(const QString & filter)
    {
        m_fileSelectionFilter = filter;
    }

    virtual QString displayName() const override
    {
        return m_displayName;
    }
    virtual QIcon icon() const override
    {
        return APPLICATION->getThemedIcon(m_iconName);
    }
    virtual QString id() const override
    {
        return m_id;
    }
    virtual QString helpPage() const override
    {
        return m_helpName;
    }
    virtual bool shouldDisplay() const override;

    virtual void openedImpl() override;
    virtual void closedImpl() override;
protected:
    bool eventFilter(QObject *obj, QEvent *ev) override;
    bool modListFilter(QKeyEvent *ev);
    QMenu * createPopupMenu() override;

protected:
    BaseInstance *m_inst = nullptr;

protected:
    Ui::ModFolderPage *ui = nullptr;
    std::shared_ptr<ModFolderModel> m_mods;
    QSortFilterProxyModel *m_filterModel = nullptr;
    QString m_iconName;
    QString m_id;
    QString m_displayName;
    QString m_helpName;
    QString m_fileSelectionFilter;
    QString m_viewFilter;
    bool m_controlsEnabled = true;

public
slots:
    void modCurrent(const QModelIndex &current, const QModelIndex &previous);

private
slots:
    void modItemActivated(const QModelIndex &index);
    void on_filterTextChanged(const QString & newContents);
    void on_RunningState_changed(bool running);
    void on_actionAdd_triggered();
    void on_actionRemove_triggered();
    void on_actionEnable_triggered();
    void on_actionDisable_triggered();
    void on_actionView_Folder_triggered();
    void on_actionView_configs_triggered();
    void ShowContextMenu(const QPoint &pos);
};

class CoreModFolderPage : public ModFolderPage
{
public:
    explicit CoreModFolderPage(BaseInstance *inst, std::shared_ptr<ModFolderModel> mods, QString id,
                               QString iconName, QString displayName, QString helpPage = "",
                               QWidget *parent = 0);
    virtual ~CoreModFolderPage()
    {
    }
    virtual bool shouldDisplay() const;
};
