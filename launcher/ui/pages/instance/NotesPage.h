/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QWidget>

#include "BaseInstance.h"
#include "ui/pages/BasePage.h"
#include <Application.h>

namespace Ui
{
class NotesPage;
}

class NotesPage : public QWidget, public BasePage
{
    Q_OBJECT

public:
    explicit NotesPage(BaseInstance *inst, QWidget *parent = 0);
    virtual ~NotesPage();
    virtual QString displayName() const override
    {
        return tr("Notes");
    }
    virtual QIcon icon() const override
    {
        auto icon = APPLICATION->getThemedIcon("notes");
        if(icon.isNull())
            icon = APPLICATION->getThemedIcon("news");
        return icon;
    }
    virtual QString id() const override
    {
        return "notes";
    }
    virtual bool apply() override;
    virtual QString helpPage() const override
    {
        return "Notes";
    }

private:
    Ui::NotesPage *ui;
    BaseInstance *m_inst;
};
