/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QWidget>

#include "minecraft/legacy/LegacyInstance.h"
#include "ui/pages/BasePage.h"
#include <Application.h>
#include "tasks/Task.h"

namespace Ui
{
class LegacyUpgradePage;
}

class LegacyUpgradePage : public QWidget, public BasePage
{
    Q_OBJECT

public:
    explicit LegacyUpgradePage(InstancePtr inst, QWidget *parent = 0);
    virtual ~LegacyUpgradePage();
    virtual QString displayName() const override
    {
        return tr("Upgrade");
    }
    virtual QIcon icon() const override
    {
        return APPLICATION->getThemedIcon("checkupdate");
    }
    virtual QString id() const override
    {
        return "upgrade";
    }
    virtual QString helpPage() const override
    {
        return "Legacy-upgrade";
    }
    virtual bool shouldDisplay() const override;

private slots:
    void on_upgradeButton_clicked();

private:
    void runModalTask(Task *task);

private:
    Ui::LegacyUpgradePage *ui;
    InstancePtr m_inst;
};
