/* Copyright 2015-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QMainWindow>

#include "minecraft/MinecraftInstance.h"
#include "ui/pages/BasePage.h"
#include <Application.h>
#include <LoggedProcess.h>

class WorldList;
namespace Ui
{
class WorldListPage;
}

class WorldListPage : public QMainWindow, public BasePage
{
    Q_OBJECT

public:
    explicit WorldListPage(
        BaseInstance *inst,
        std::shared_ptr<WorldList> worlds,
        QWidget *parent = 0
    );
    virtual ~WorldListPage();

    virtual QString displayName() const override
    {
        return tr("Worlds");
    }
    virtual QIcon icon() const override
    {
        return APPLICATION->getThemedIcon("worlds");
    }
    virtual QString id() const override
    {
        return "worlds";
    }
    virtual QString helpPage() const override
    {
        return "Worlds";
    }
    virtual bool shouldDisplay() const override;

    virtual void openedImpl() override;
    virtual void closedImpl() override;

protected:
    bool eventFilter(QObject *obj, QEvent *ev) override;
    bool worldListFilter(QKeyEvent *ev);
    QMenu * createPopupMenu() override;

protected:
    BaseInstance *m_inst;

private:
    QModelIndex getSelectedWorld();
    bool isWorldSafe(QModelIndex index);
    bool worldSafetyNagQuestion();
    void mceditError();

private:
    Ui::WorldListPage *ui;
    std::shared_ptr<WorldList> m_worlds;
    unique_qobject_ptr<LoggedProcess> m_mceditProcess;
    bool m_mceditStarting = false;

private slots:
    void on_actionCopy_Seed_triggered();
    void on_actionMCEdit_triggered();
    void on_actionRemove_triggered();
    void on_actionAdd_triggered();
    void on_actionCopy_triggered();
    void on_actionRename_triggered();
    void on_actionRefresh_triggered();
    void on_actionView_Folder_triggered();
    void on_actionDatapacks_triggered();
    void on_actionReset_Icon_triggered();
    void worldChanged(const QModelIndex &current, const QModelIndex &previous);
    void mceditState(LoggedProcess::State state);

    void ShowContextMenu(const QPoint &pos);
};
