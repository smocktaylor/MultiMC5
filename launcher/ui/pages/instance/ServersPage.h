/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QMainWindow>
#include <QString>

#include "ui/pages/BasePage.h"
#include <Application.h>

namespace Ui
{
class ServersPage;
}

struct Server;
class ServersModel;
class MinecraftInstance;

class ServersPage : public QMainWindow, public BasePage
{
    Q_OBJECT

public:
    explicit ServersPage(InstancePtr inst, QWidget *parent = 0);
    virtual ~ServersPage();

    void openedImpl() override;
    void closedImpl() override;

    virtual QString displayName() const override
    {
        return tr("Servers");
    }
    virtual QIcon icon() const override
    {
        return APPLICATION->getThemedIcon("unknown_server");
    }
    virtual QString id() const override
    {
        return "servers";
    }
    virtual QString helpPage() const override
    {
        return "Servers-management";
    }

protected:
    QMenu * createPopupMenu() override;

private:
    void updateState();
    void scheduleSave();
    bool saveIsScheduled() const;

private slots:
    void currentChanged(const QModelIndex &current, const QModelIndex &previous);
    void rowsRemoved(const QModelIndex &parent, int first, int last);

    void on_actionAdd_triggered();
    void on_actionRemove_triggered();
    void on_actionMove_Up_triggered();
    void on_actionMove_Down_triggered();
    void on_actionJoin_triggered();

    void on_RunningState_changed(bool running);

    void nameEdited(const QString & name);
    void addressEdited(const QString & address);
    void resourceIndexChanged(int index);\

    void ShowContextMenu(const QPoint &pos);

private: // data
    int currentServer = -1;
    bool m_locked = true;
    Ui::ServersPage *ui = nullptr;
    ServersModel * m_model = nullptr;
    InstancePtr m_inst = nullptr;
};

