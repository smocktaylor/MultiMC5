/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QWidget>

#include "ui/pages/BasePage.h"
#include <Application.h>

namespace Ui {
class ExternalToolsPage;
}

class ExternalToolsPage : public QWidget, public BasePage
{
    Q_OBJECT

public:
    explicit ExternalToolsPage(QWidget *parent = 0);
    ~ExternalToolsPage();

    QString displayName() const override
    {
        return tr("External Tools");
    }
    QIcon icon() const override
    {
        auto icon = APPLICATION->getThemedIcon("externaltools");
        if(icon.isNull())
        {
            icon = APPLICATION->getThemedIcon("loadermods");
        }
        return icon;
    }
    QString id() const override
    {
        return "external-tools";
    }
    QString helpPage() const override
    {
        return "Tools";
    }
    virtual bool apply() override;

private:
    void loadSettings();
    void applySettings();

private:
    Ui::ExternalToolsPage *ui;

private
slots:
    void on_jprofilerPathBtn_clicked();
    void on_jprofilerCheckBtn_clicked();
    void on_jvisualvmPathBtn_clicked();
    void on_jvisualvmCheckBtn_clicked();
    void on_mceditPathBtn_clicked();
    void on_mceditCheckBtn_clicked();
    void on_jsonEditorBrowseBtn_clicked();
};
