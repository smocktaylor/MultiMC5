/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <memory>
#include <QDialog>

#include "java/JavaChecker.h"
#include "ui/pages/BasePage.h"
#include <Application.h>

class SettingsObject;

namespace Ui
{
class MinecraftPage;
}

class MinecraftPage : public QWidget, public BasePage
{
    Q_OBJECT

public:
    explicit MinecraftPage(QWidget *parent = 0);
    ~MinecraftPage();

    QString displayName() const override
    {
        return tr("Minecraft");
    }
    QIcon icon() const override
    {
        return APPLICATION->getThemedIcon("minecraft");
    }
    QString id() const override
    {
        return "minecraft-settings";
    }
    QString helpPage() const override
    {
        return "Minecraft-settings";
    }
    bool apply() override;

private:
    void updateCheckboxStuff();
    void applySettings();
    void loadSettings();

private
slots:
    void on_maximizedCheckBox_clicked(bool checked);

private:
    Ui::MinecraftPage *ui;

};
