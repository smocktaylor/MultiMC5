/* Copyright 2018-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <memory>
#include <QDialog>

#include "ui/pages/BasePage.h"
#include <Application.h>
#include "ui/widgets/CustomCommands.h"

class CustomCommandsPage : public QWidget, public BasePage
{
    Q_OBJECT

public:
    explicit CustomCommandsPage(QWidget *parent = 0);
    ~CustomCommandsPage();

    QString displayName() const override
    {
        return tr("Custom Commands");
    }
    QIcon icon() const override
    {
        return APPLICATION->getThemedIcon("custom-commands");
    }
    QString id() const override
    {
        return "custom-commands";
    }
    QString helpPage() const override
    {
        return "Custom-commands";
    }
    bool apply() override;

private:
    void applySettings();
    void loadSettings();
    CustomCommands * commands;
};
