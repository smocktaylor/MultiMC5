/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <memory>
#include <QDialog>
#include "ui/pages/BasePage.h"
#include "JavaCommon.h"
#include <Application.h>
#include <QObjectPtr.h>

class SettingsObject;

namespace Ui
{
class JavaPage;
}

class JavaPage : public QWidget, public BasePage
{
    Q_OBJECT

public:
    explicit JavaPage(QWidget *parent = 0);
    ~JavaPage();

    QString displayName() const override
    {
        return tr("Java");
    }
    QIcon icon() const override
    {
        return APPLICATION->getThemedIcon("java");
    }
    QString id() const override
    {
        return "java-settings";
    }
    QString helpPage() const override
    {
        return "Java-settings";
    }
    bool apply() override;

private:
    void applySettings();
    void loadSettings();

private
slots:
    void on_javaDetectBtn_clicked();
    void on_javaTestBtn_clicked();
    void on_javaBrowseBtn_clicked();
    void checkerFinished();

private:
    Ui::JavaPage *ui;
    unique_qobject_ptr<JavaCommon::TestCheck> checker;
};
