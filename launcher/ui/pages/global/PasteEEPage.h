/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QWidget>

#include "ui/pages/BasePage.h"
#include <Application.h>

namespace Ui {
class PasteEEPage;
}

class PasteEEPage : public QWidget, public BasePage
{
    Q_OBJECT

public:
    explicit PasteEEPage(QWidget *parent = 0);
    ~PasteEEPage();

    QString displayName() const override
    {
        return tr("Log Upload");
    }
    QIcon icon() const override
    {
        return APPLICATION->getThemedIcon("log");
    }
    QString id() const override
    {
        return "log-upload";
    }
    QString helpPage() const override
    {
        return "Log-Upload";
    }
    virtual bool apply() override;

private:
    void loadSettings();
    void applySettings();

private slots:
    void textEdited(const QString &text);

private:
    Ui::PasteEEPage *ui;
};
