/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <memory>
#include "ui/pages/BasePage.h"
#include <Application.h>
#include <QWidget>

class LanguageSelectionWidget;

class LanguagePage : public QWidget, public BasePage
{
    Q_OBJECT

public:
    explicit LanguagePage(QWidget *parent = 0);
    virtual ~LanguagePage();

    QString displayName() const override
    {
        return tr("Language");
    }
    QIcon icon() const override
    {
        return APPLICATION->getThemedIcon("language");
    }
    QString id() const override
    {
        return "language-settings";
    }
    QString helpPage() const override
    {
        return "Language-settings";
    }
    bool apply() override;

    void changeEvent(QEvent * ) override;

private:
    void applySettings();
    void loadSettings();
    void retranslate();

private:
    LanguageSelectionWidget *mainWidget;
};
