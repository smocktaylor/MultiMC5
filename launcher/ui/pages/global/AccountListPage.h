/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QMainWindow>
#include <memory>

#include "ui/pages/BasePage.h"

#include "minecraft/auth/AccountList.h"
#include "Application.h"

namespace Ui
{
class AccountListPage;
}

class AuthenticateTask;

class AccountListPage : public QMainWindow, public BasePage
{
    Q_OBJECT
public:
    explicit AccountListPage(QWidget *parent = 0);
    ~AccountListPage();

    QString displayName() const override
    {
        return tr("Accounts");
    }
    QIcon icon() const override
    {
        auto icon = APPLICATION->getThemedIcon("accounts");
        if(icon.isNull())
        {
            icon = APPLICATION->getThemedIcon("noaccount");
        }
        return icon;
    }
    QString id() const override
    {
        return "accounts";
    }
    QString helpPage() const override
    {
        return "Getting-Started#adding-an-account";
    }

public slots:
    void on_actionAddMojang_triggered();
    void on_actionAddMicrosoft_triggered();
    void on_actionRemove_triggered();
    void on_actionRefresh_triggered();
    void on_actionSetDefault_triggered();
    void on_actionNoDefault_triggered();
    void on_actionUploadSkin_triggered();
    void on_actionDeleteSkin_triggered();

    void listChanged();

    //! Updates the states of the dialog's buttons.
    void updateButtonStates();

protected slots:
    void ShowContextMenu(const QPoint &pos);

private:
    void changeEvent(QEvent * event) override;
    QMenu * createPopupMenu() override;
    shared_qobject_ptr<AccountList> m_accounts;
    Ui::AccountListPage *ui;
};
