/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <memory>
#include <QDialog>

#include "ui/pages/BasePage.h"
#include <Application.h>

namespace Ui
{
class ProxyPage;
}

class ProxyPage : public QWidget, public BasePage
{
    Q_OBJECT

public:
    explicit ProxyPage(QWidget *parent = 0);
    ~ProxyPage();

    QString displayName() const override
    {
        return tr("Proxy");
    }
    QIcon icon() const override
    {
        return APPLICATION->getThemedIcon("proxy");
    }
    QString id() const override
    {
        return "proxy-settings";
    }
    QString helpPage() const override
    {
        return "Proxy-settings";
    }
    bool apply() override;

private:
    void updateCheckboxStuff();
    void applySettings();
    void loadSettings();

private
slots:
    void proxyChanged(int);

private:
    Ui::ProxyPage *ui;
};
