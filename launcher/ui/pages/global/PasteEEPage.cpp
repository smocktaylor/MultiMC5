/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#include "PasteEEPage.h"
#include "ui_PasteEEPage.h"

#include <QMessageBox>
#include <QFileDialog>
#include <QStandardPaths>
#include <QTabBar>

#include "settings/SettingsObject.h"
#include "tools/BaseProfiler.h"
#include "Application.h"

PasteEEPage::PasteEEPage(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PasteEEPage)
{
    ui->setupUi(this);
    ui->tabWidget->tabBar()->hide();\
    connect(ui->customAPIkeyEdit, &QLineEdit::textEdited, this, &PasteEEPage::textEdited);
    loadSettings();
}

PasteEEPage::~PasteEEPage()
{
    delete ui;
}

void PasteEEPage::loadSettings()
{
    auto s = APPLICATION->settings();
    QString keyToUse = s->get("PasteEEAPIKey").toString();
    if(keyToUse == "multimc")
    {
        ui->multimcButton->setChecked(true);
    }
    else
    {
        ui->customButton->setChecked(true);
        ui->customAPIkeyEdit->setText(keyToUse);
    }
}

void PasteEEPage::applySettings()
{
    auto s = APPLICATION->settings();

    QString pasteKeyToUse;
    if (ui->customButton->isChecked())
        pasteKeyToUse = ui->customAPIkeyEdit->text();
    else
    {
        pasteKeyToUse =  "multimc";
    }
    s->set("PasteEEAPIKey", pasteKeyToUse);
}

bool PasteEEPage::apply()
{
    applySettings();
    return true;
}

void PasteEEPage::textEdited(const QString& text)
{
    ui->customButton->setChecked(true);
}
