/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QWidget>

#include "ui/pages/BasePage.h"
#include <Application.h>
#include "tasks/Task.h"
#include <modplatform/flame/FlamePackIndex.h>

namespace Ui
{
class FlamePage;
}

class NewInstanceDialog;

namespace Flame {
    class ListModel;
}

class FlamePage : public QWidget, public BasePage
{
    Q_OBJECT

public:
    explicit FlamePage(NewInstanceDialog* dialog, QWidget *parent = 0);
    virtual ~FlamePage();
    virtual QString displayName() const override
    {
        return tr("CurseForge");
    }
    virtual QIcon icon() const override
    {
        return APPLICATION->getThemedIcon("flame");
    }
    virtual QString id() const override
    {
        return "flame";
    }
    virtual QString helpPage() const override
    {
        return "Flame-platform";
    }
    virtual bool shouldDisplay() const override;

    void openedImpl() override;

    bool eventFilter(QObject * watched, QEvent * event) override;

private:
    void suggestCurrent();

private slots:
    void triggerSearch();
    void onSelectionChanged(QModelIndex first, QModelIndex second);
    void onVersionSelectionChanged(QString data);

private:
    Ui::FlamePage *ui = nullptr;
    NewInstanceDialog* dialog = nullptr;
    Flame::ListModel* listModel = nullptr;
    Flame::IndexedPack current;

    QString selectedVersion;
};
