/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include "FtbFilterModel.h"
#include "FtbListModel.h"

#include <QWidget>

#include "Application.h"
#include "ui/pages/BasePage.h"
#include "tasks/Task.h"

namespace Ui
{
    class FtbPage;
}

class NewInstanceDialog;

class FtbPage : public QWidget, public BasePage
{
Q_OBJECT

public:
    explicit FtbPage(NewInstanceDialog* dialog, QWidget *parent = 0);
    virtual ~FtbPage();
    virtual QString displayName() const override
    {
        return tr("FTB");
    }
    virtual QIcon icon() const override
    {
        return APPLICATION->getThemedIcon("ftb_logo");
    }
    virtual QString id() const override
    {
        return "ftb";
    }
    virtual QString helpPage() const override
    {
        return "FTB-platform";
    }
    virtual bool shouldDisplay() const override;

    void openedImpl() override;

    bool eventFilter(QObject * watched, QEvent * event) override;

private:
    void suggestCurrent();

private slots:
    void triggerSearch();

    void onSortingSelectionChanged(QString data);
    void onSelectionChanged(QModelIndex first, QModelIndex second);
    void onVersionSelectionChanged(QString data);

private:
    Ui::FtbPage *ui = nullptr;
    NewInstanceDialog* dialog = nullptr;
    Ftb::ListModel* listModel = nullptr;
    Ftb::FilterModel* filterModel = nullptr;

    ModpacksCH::Modpack selected;
    QString selectedVersion;

    bool initialised { false };
};
