/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QWidget>

#include "ui/pages/BasePage.h"
#include <Application.h>
#include "tasks/Task.h"
#include "TechnicData.h"

namespace Ui
{
class TechnicPage;
}

class NewInstanceDialog;

namespace Technic {
    class ListModel;
}

class TechnicPage : public QWidget, public BasePage
{
    Q_OBJECT

public:
    explicit TechnicPage(NewInstanceDialog* dialog, QWidget *parent = 0);
    virtual ~TechnicPage();
    virtual QString displayName() const override
    {
        return tr("Technic");
    }
    virtual QIcon icon() const override
    {
        return APPLICATION->getThemedIcon("technic");
    }
    virtual QString id() const override
    {
        return "technic";
    }
    virtual QString helpPage() const override
    {
        return "Technic-platform";
    }
    virtual bool shouldDisplay() const override;

    void openedImpl() override;

    bool eventFilter(QObject* watched, QEvent* event) override;

private:
    void suggestCurrent();
    void metadataLoaded();

private slots:
    void triggerSearch();
    void onSelectionChanged(QModelIndex first, QModelIndex second);

private:
    Ui::TechnicPage *ui = nullptr;
    NewInstanceDialog* dialog = nullptr;
    Technic::ListModel* model = nullptr;
    Technic::Modpack current;
};
