/* Copyright 2020-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QList>
#include <QString>

namespace Technic {
struct Modpack {
    QString slug;

    QString name;
    QString logoUrl;
    QString logoName;

    bool broken = true;

    QString url;
    bool isSolder = false;
    QString minecraftVersion;

    bool metadataLoaded = false;
    QString websiteUrl;
    QString author;
    QString description;
};
}

Q_DECLARE_METATYPE(Technic::Modpack)
