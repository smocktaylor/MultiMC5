/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QWidget>

#include "ui/pages/BasePage.h"
#include <Application.h>
#include "tasks/Task.h"

namespace Ui
{
class VanillaPage;
}

class NewInstanceDialog;

class VanillaPage : public QWidget, public BasePage
{
    Q_OBJECT

public:
    explicit VanillaPage(NewInstanceDialog *dialog, QWidget *parent = 0);
    virtual ~VanillaPage();
    virtual QString displayName() const override
    {
        return tr("Vanilla");
    }
    virtual QIcon icon() const override
    {
        return APPLICATION->getThemedIcon("minecraft");
    }
    virtual QString id() const override
    {
        return "vanilla";
    }
    virtual QString helpPage() const override
    {
        return "Vanilla-platform";
    }
    virtual bool shouldDisplay() const override;
    void openedImpl() override;

    BaseVersionPtr selectedVersion() const;

public slots:
    void setSelectedVersion(BaseVersionPtr version);

private slots:
    void filterChanged();

private:
    void refresh();
    void suggestCurrent();

private:
    bool initialized = false;
    NewInstanceDialog *dialog = nullptr;
    Ui::VanillaPage *ui = nullptr;
    bool m_versionSetByUser = false;
    BaseVersionPtr m_selectedVersion;
};
