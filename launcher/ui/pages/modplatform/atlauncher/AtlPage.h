/*
 * Copyright 2020-2021 Jamie Mansfield <jmansfield@cadixdev.org>
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include "AtlFilterModel.h"
#include "AtlListModel.h"

#include <QWidget>
#include <modplatform/atlauncher/ATLPackInstallTask.h>

#include "Application.h"
#include "ui/pages/BasePage.h"
#include "tasks/Task.h"

namespace Ui
{
    class AtlPage;
}

class NewInstanceDialog;

class AtlPage : public QWidget, public BasePage, public ATLauncher::UserInteractionSupport
{
Q_OBJECT

public:
    explicit AtlPage(NewInstanceDialog* dialog, QWidget *parent = 0);
    virtual ~AtlPage();
    virtual QString displayName() const override
    {
        return tr("ATLauncher");
    }
    virtual QIcon icon() const override
    {
        return APPLICATION->getThemedIcon("atlauncher");
    }
    virtual QString id() const override
    {
        return "atl";
    }
    virtual QString helpPage() const override
    {
        return "ATL-platform";
    }
    virtual bool shouldDisplay() const override;

    void openedImpl() override;

private:
    void suggestCurrent();

    QString chooseVersion(Meta::VersionListPtr vlist, QString minecraftVersion) override;
    QVector<QString> chooseOptionalMods(QVector<ATLauncher::VersionMod> mods) override;

private slots:
    void triggerSearch();

    void onSortingSelectionChanged(QString data);

    void onSelectionChanged(QModelIndex first, QModelIndex second);
    void onVersionSelectionChanged(QString data);

private:
    Ui::AtlPage *ui = nullptr;
    NewInstanceDialog* dialog = nullptr;
    Atl::ListModel* listModel = nullptr;
    Atl::FilterModel* filterModel = nullptr;

    ATLauncher::IndexedPack selected;
    QString selectedVersion;

    bool initialized = false;
};
