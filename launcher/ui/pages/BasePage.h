/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QString>
#include <QIcon>
#include <memory>

#include "BasePageContainer.h"

class BasePage
{
public:
    virtual ~BasePage() {}
    virtual QString id() const = 0;
    virtual QString displayName() const = 0;
    virtual QIcon icon() const = 0;
    virtual bool apply() { return true; }
    virtual bool shouldDisplay() const { return true; }
    virtual QString helpPage() const { return QString(); }
    void opened()
    {
        isOpened = true;
        openedImpl();
    }
    void closed()
    {
        isOpened = false;
        closedImpl();
    }
    virtual void openedImpl() {}
    virtual void closedImpl() {}
    virtual void setParentContainer(BasePageContainer * container)
    {
        m_container = container;
    };
public:
    int stackIndex = -1;
    int listIndex = -1;
protected:
    BasePageContainer * m_container = nullptr;
    bool isOpened = false;
};

typedef std::shared_ptr<BasePage> BasePagePtr;
