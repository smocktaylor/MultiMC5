/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include "InstanceTask.h"
#include "net/NetJob.h"
#include <QUrl>
#include <QFuture>
#include <QFutureWatcher>
#include "settings/SettingsObject.h"
#include "QObjectPtr.h"

#include <nonstd/optional>

class QuaZip;
namespace Flame
{
    class FileResolvingTask;
}

class InstanceImportTask : public InstanceTask
{
    Q_OBJECT
public:
    explicit InstanceImportTask(const QUrl sourceUrl);

protected:
    //! Entry point for tasks.
    virtual void executeTask() override;

private:
    void processZipPack();
    void processMultiMC();
    void processFlame();
    void processTechnic();

private slots:
    void downloadSucceeded();
    void downloadFailed(QString reason);
    void downloadProgressChanged(qint64 current, qint64 total);
    void extractFinished();
    void extractAborted();

private: /* data */
    NetJob::Ptr m_filesNetJob;
    shared_qobject_ptr<Flame::FileResolvingTask> m_modIdResolver;
    QUrl m_sourceUrl;
    QString m_archivePath;
    bool m_downloadRequired = false;
    std::unique_ptr<QuaZip> m_packZip;
    QFuture<nonstd::optional<QStringList>> m_extractFuture;
    QFutureWatcher<nonstd::optional<QStringList>> m_extractFutureWatcher;
    enum class ModpackType{
        Unknown,
        MultiMC,
        Flame,
        Technic
    } m_modpackType = ModpackType::Unknown;
};
