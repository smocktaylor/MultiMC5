/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QString>
#include <QDebug>
#include <exception>

class Exception : public std::exception
{
public:
    Exception(const QString &message) : std::exception(), m_message(message)
    {
        qCritical() << "Exception:" << message;
    }
    Exception(const Exception &other)
        : std::exception(), m_message(other.cause())
    {
    }
    virtual ~Exception() noexcept {}
    const char *what() const noexcept
    {
        return m_message.toLatin1().constData();
    }
    QString cause() const
    {
        return m_message;
    }

private:
    QString m_message;
};
