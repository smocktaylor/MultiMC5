/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <memory>

class MinecraftInstance;
class QDir;
class QString;
class QObject;
class Task;
class BaseVersion;
typedef std::shared_ptr<BaseVersion> BaseVersionPtr;

class BaseInstaller
{
public:
    BaseInstaller();
    virtual ~BaseInstaller(){};
    bool isApplied(MinecraftInstance *on);

    virtual bool add(MinecraftInstance *to);
    virtual bool remove(MinecraftInstance *from);

    virtual Task *createInstallTask(MinecraftInstance *instance, BaseVersionPtr version, QObject *parent) = 0;

protected:
    virtual QString id() const = 0;
    QString filename(const QString &root) const;
    QDir patchesDir(const QString &root) const;
};
