/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <launch/LaunchStep.h>
#include <memory>

class ReconstructAssets: public LaunchStep
{
    Q_OBJECT
public:
    explicit ReconstructAssets(LaunchTask *parent) : LaunchStep(parent){};
    virtual ~ReconstructAssets(){};

    void executeTask() override;
    bool canAbort() const override
    {
        return false;
    }
};
