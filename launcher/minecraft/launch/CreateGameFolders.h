/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <launch/LaunchStep.h>
#include <LoggedProcess.h>
#include <minecraft/auth/AuthSession.h>

// Create the main .minecraft for the instance and any other necessary folders
class CreateGameFolders: public LaunchStep
{
    Q_OBJECT
public:
    explicit CreateGameFolders(LaunchTask *parent);
    virtual ~CreateGameFolders() {};

    virtual void executeTask();
    virtual bool canAbort() const
    {
        return false;
    }
};


