/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <launch/LaunchStep.h>
#include <LoggedProcess.h>
#include <minecraft/auth/AuthSession.h>

#include "MinecraftServerTarget.h"

class LauncherPartLaunch: public LaunchStep
{
    Q_OBJECT
public:
    explicit LauncherPartLaunch(LaunchTask *parent);
    virtual ~LauncherPartLaunch() {};

    virtual void executeTask();
    virtual bool abort();
    virtual void proceed();
    virtual bool canAbort() const
    {
        return true;
    }
    void setWorkingDirectory(const QString &wd);
    void setAuthSession(AuthSessionPtr session)
    {
        m_session = session;
    }

    void setServerToJoin(MinecraftServerTargetPtr serverToJoin)
    {
        m_serverToJoin = std::move(serverToJoin);
    }

private slots:
    void on_state(LoggedProcess::State state);

private:
    LoggedProcess m_process;
    QString m_command;
    AuthSessionPtr m_session;
    QString m_launchScript;
    MinecraftServerTargetPtr m_serverToJoin;

    bool mayProceed = false;
};
