/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <launch/LaunchStep.h>
#include <memory>
#include "minecraft/auth/AuthSession.h"

// FIXME: temporary wrapper for existing task.
class ExtractNatives: public LaunchStep
{
    Q_OBJECT
public:
    explicit ExtractNatives(LaunchTask *parent) : LaunchStep(parent){};
    virtual ~ExtractNatives(){};

    void executeTask() override;
    bool canAbort() const override
    {
        return false;
    }
    void finalize() override;
};


