/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <launch/LaunchStep.h>
#include <memory>

class ModMinecraftJar: public LaunchStep
{
    Q_OBJECT
public:
    explicit ModMinecraftJar(LaunchTask *parent) : LaunchStep(parent) {};
    virtual ~ModMinecraftJar(){};

    virtual void executeTask() override;
    virtual bool canAbort() const override
    {
        return false;
    }
    void finalize() override;
private:
    bool removeJar();
};
