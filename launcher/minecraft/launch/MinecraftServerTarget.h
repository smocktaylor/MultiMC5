/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */


#pragma once

#include <memory>

#include <QString>

struct MinecraftServerTarget {
    QString address;
    quint16 port;

    static MinecraftServerTarget parse(const QString &fullAddress);
};

typedef std::shared_ptr<MinecraftServerTarget> MinecraftServerTargetPtr;
