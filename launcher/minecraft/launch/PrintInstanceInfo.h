/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <launch/LaunchStep.h>
#include <memory>
#include "minecraft/auth/AuthSession.h"
#include "minecraft/launch/MinecraftServerTarget.h"

// FIXME: temporary wrapper for existing task.
class PrintInstanceInfo: public LaunchStep
{
    Q_OBJECT
public:
    explicit PrintInstanceInfo(LaunchTask *parent, AuthSessionPtr session, MinecraftServerTargetPtr serverToJoin) :
        LaunchStep(parent), m_session(session), m_serverToJoin(serverToJoin) {};
    virtual ~PrintInstanceInfo(){};

    virtual void executeTask();
    virtual bool canAbort() const
    {
        return false;
    }
private:
    AuthSessionPtr m_session;
    MinecraftServerTargetPtr m_serverToJoin;
};

