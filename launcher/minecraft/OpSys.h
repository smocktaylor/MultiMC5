/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once
#include <QString>
enum OpSys
{
    Os_Windows,
    Os_FreeBSD,
    Os_Linux,
    Os_OSX,
    Os_Other
};

OpSys OpSys_fromString(QString);
QString OpSys_toString(OpSys);

#ifdef Q_OS_WIN32
    #define currentSystem Os_Windows
#elif defined Q_OS_MAC
    #define currentSystem Os_OSX
#elif defined Q_OS_FREEBSD
    #define currentSystem Os_FreeBSD
#else
    #define currentSystem Os_Linux
#endif
