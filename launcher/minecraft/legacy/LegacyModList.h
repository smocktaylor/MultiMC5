/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QList>
#include <QString>
#include <QDir>

class LegacyModList
{
public:

    using Mod = QFileInfo;

    LegacyModList(const QString &dir, const QString &list_file = QString());

    /// Reloads the mod list and returns true if the list changed.
    bool update();

    QDir dir()
    {
        return m_dir;
    }

    const QList<Mod> & allMods()
    {
        return mods;
    }

protected:
    QDir m_dir;
    QString m_list_file;
    QList<Mod> mods;
};
