/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QObject>
#include <QList>
#include <QUrl>

#include "tasks/Task.h"
#include <quazip.h>

#include "QObjectPtr.h"

class MinecraftVersion;
class MinecraftInstance;

class MinecraftLoadAndCheck : public Task
{
    Q_OBJECT
public:
    explicit MinecraftLoadAndCheck(MinecraftInstance *inst, QObject *parent = 0);
    virtual ~MinecraftLoadAndCheck() {};
    void executeTask() override;

private slots:
    void subtaskSucceeded();
    void subtaskFailed(QString error);

private:
    MinecraftInstance *m_inst = nullptr;
    Task::Ptr m_task;
    QString m_preFailure;
    QString m_fail_reason;
};

