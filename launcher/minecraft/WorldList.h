/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */


#pragma once

#include <QList>
#include <QString>
#include <QDir>
#include <QAbstractListModel>
#include <QMimeData>
#include "minecraft/World.h"

class QFileSystemWatcher;

class WorldList : public QAbstractListModel
{
    Q_OBJECT
public:
    enum Columns
    {
        NameColumn,
        GameModeColumn,
        LastPlayedColumn
    };

    enum Roles
    {
        ObjectRole = Qt::UserRole + 1,
        FolderRole,
        SeedRole,
        NameRole,
        GameModeRole,
        LastPlayedRole,
        IconFileRole
    };

    WorldList(const QString &dir);

    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const
    {
        return size();
    };
    virtual QVariant headerData(int section, Qt::Orientation orientation,
                                int role = Qt::DisplayRole) const;
    virtual int columnCount(const QModelIndex &parent) const;

    size_t size() const
    {
        return worlds.size();
    };
    bool empty() const
    {
        return size() == 0;
    }
    World &operator[](size_t index)
    {
        return worlds[index];
    }

    /// Reloads the mod list and returns true if the list changed.
    virtual bool update();

    /// Install a world from location
    void installWorld(QFileInfo filename);

    /// Deletes the mod at the given index.
    virtual bool deleteWorld(int index);

    /// Removes the world icon, if any
    virtual bool resetIcon(int index);

    /// Deletes all the selected mods
    virtual bool deleteWorlds(int first, int last);

    /// flags, mostly to support drag&drop
    virtual Qt::ItemFlags flags(const QModelIndex &index) const;
    /// get data for drag action
    virtual QMimeData *mimeData(const QModelIndexList &indexes) const;
    /// get the supported mime types
    virtual QStringList mimeTypes() const;
    /// process data from drop action
    virtual bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent);
    /// what drag actions do we support?
    virtual Qt::DropActions supportedDragActions() const;

    /// what drop actions do we support?
    virtual Qt::DropActions supportedDropActions() const;

    void startWatching();
    void stopWatching();

    virtual bool isValid();

    QDir dir() const
    {
        return m_dir;
    }

    const QList<World> &allWorlds() const
    {
        return worlds;
    }

private slots:
    void directoryChanged(QString path);

signals:
    void changed();

protected:
    QFileSystemWatcher *m_watcher;
    bool is_watching;
    QDir m_dir;
    QList<World> worlds;
};
