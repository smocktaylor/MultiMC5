/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#include "OpSys.h"

OpSys OpSys_fromString(QString name)
{
    if (name == "freebsd")
        return Os_FreeBSD;
    if (name == "linux")
        return Os_Linux;
    if (name == "windows")
        return Os_Windows;
    if (name == "osx")
        return Os_OSX;
    return Os_Other;
}

QString OpSys_toString(OpSys name)
{
    switch (name)
    {
    case Os_FreeBSD:
	return "freebsd";
    case Os_Linux:
        return "linux";
    case Os_OSX:
        return "osx";
    case Os_Windows:
        return "windows";
    default:
        return "other";
    }
}
