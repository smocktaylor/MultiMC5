/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QObject>
#include <QList>
#include <QUrl>

#include "net/NetJob.h"
#include "tasks/Task.h"
#include "minecraft/VersionFilterData.h"
#include <quazip.h>

class MinecraftVersion;
class MinecraftInstance;

class MinecraftUpdate : public Task
{
    Q_OBJECT
public:
    explicit MinecraftUpdate(MinecraftInstance *inst, QObject *parent = 0);
    virtual ~MinecraftUpdate() {};

    void executeTask() override;
    bool canAbort() const override;

private
slots:
    bool abort() override;
    void subtaskSucceeded();
    void subtaskFailed(QString error);

private:
    void next();

private:
    MinecraftInstance *m_inst = nullptr;
    QList<std::shared_ptr<Task>> m_tasks;
    QString m_preFailure;
    int m_currentTask = -1;
    bool m_abort = false;
    bool m_failed_out_of_order = false;
    QString m_fail_reason;
};
