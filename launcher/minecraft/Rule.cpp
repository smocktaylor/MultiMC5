/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#include <QJsonObject>
#include <QJsonArray>

#include "Rule.h"

RuleAction RuleAction_fromString(QString name)
{
    if (name == "allow")
        return Allow;
    if (name == "disallow")
        return Disallow;
    return Defer;
}

QList<std::shared_ptr<Rule>> rulesFromJsonV4(const QJsonObject &objectWithRules)
{
    QList<std::shared_ptr<Rule>> rules;
    auto rulesVal = objectWithRules.value("rules");
    if (!rulesVal.isArray())
        return rules;

    QJsonArray ruleList = rulesVal.toArray();
    for (auto ruleVal : ruleList)
    {
        std::shared_ptr<Rule> rule;
        if (!ruleVal.isObject())
            continue;
        auto ruleObj = ruleVal.toObject();
        auto actionVal = ruleObj.value("action");
        if (!actionVal.isString())
            continue;
        auto action = RuleAction_fromString(actionVal.toString());
        if (action == Defer)
            continue;

        auto osVal = ruleObj.value("os");
        if (!osVal.isObject())
        {
            // add a new implicit action rule
            rules.append(ImplicitRule::create(action));
            continue;
        }

        auto osObj = osVal.toObject();
        auto osNameVal = osObj.value("name");
        if (!osNameVal.isString())
            continue;
        OpSys requiredOs = OpSys_fromString(osNameVal.toString());
        QString versionRegex = osObj.value("version").toString();
        // add a new OS rule
        rules.append(OsRule::create(action, requiredOs, versionRegex));
    }
    return rules;
}

QJsonObject ImplicitRule::toJson()
{
    QJsonObject ruleObj;
    ruleObj.insert("action", m_result == Allow ? QString("allow") : QString("disallow"));
    return ruleObj;
}

QJsonObject OsRule::toJson()
{
    QJsonObject ruleObj;
    ruleObj.insert("action", m_result == Allow ? QString("allow") : QString("disallow"));
    QJsonObject osObj;
    {
        osObj.insert("name", OpSys_toString(m_system));
        if(!m_version_regexp.isEmpty())
        {
            osObj.insert("version", m_version_regexp);
        }
    }
    ruleObj.insert("os", osObj);
    return ruleObj;
}

