/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QPixmap>

namespace SkinUtils
{
QPixmap getFaceFromCache(QString id, int height = 64, int width = 64);
}
