/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once

#include <QtCore/QtGlobal>

#ifdef CLASSPARSER_LIBRARY
#define CLASSPARSER_EXPORT Q_DECL_EXPORT
#else
#define CLASSPARSER_EXPORT Q_DECL_IMPORT
#endif
