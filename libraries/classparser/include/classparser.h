/* Copyright 2013-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

#pragma once
#include <QString>
#include "classparser_config.h"

namespace classparser
{
/**
 * @brief Get the version from a minecraft.jar by parsing its class files. Expensive!
 */
QString GetMinecraftJarVersion(QString jar);
}
