#pragma once

#include <QtCore/QtGlobal>

#ifdef RAINBOW_STATIC
    #define RAINBOW_EXPORT
#else
    #ifdef RAINBOW_LIBRARY
        #define RAINBOW_EXPORT Q_DECL_EXPORT
    #else
        #define RAINBOW_EXPORT Q_DECL_IMPORT
    #endif
#endif
