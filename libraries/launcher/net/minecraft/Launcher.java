/*
 * Copyright 2012-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

package net.minecraft;

import java.util.TreeMap;
import java.util.Map;
import java.net.URL;
import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.applet.Applet;
import java.applet.AppletStub;
import java.net.MalformedURLException;

public class Launcher extends Applet implements AppletStub
{
    private Applet wrappedApplet;
    private URL documentBase;
    private boolean active = false;
    private final Map<String, String> params;

    public Launcher(Applet applet, URL documentBase)
    {
        params = new TreeMap<String, String>();

        this.setLayout(new BorderLayout());
        this.add(applet, "Center");
        this.wrappedApplet = applet;
        this.documentBase = documentBase;
    }

    public void setParameter(String name, String value)
    {
        params.put(name, value);
    }

    public void replace(Applet applet)
    {
        this.wrappedApplet = applet;

        applet.setStub(this);
        applet.setSize(getWidth(), getHeight());

        this.setLayout(new BorderLayout());
        this.add(applet, "Center");

        applet.init();
        active = true;
        applet.start();
        validate();
    }

    @Override
    public String getParameter(String name)
    {
        String param = params.get(name);
        if (param != null)
            return param;
        try
        {
            return super.getParameter(name);
        } catch (Exception ignore){}
        return null;
    }

    @Override
    public boolean isActive()
    {
        return active;
    }

    @Override
    public void appletResize(int width, int height)
    {
        wrappedApplet.resize(width, height);
    }

    @Override
    public void resize(int width, int height)
    {
        wrappedApplet.resize(width, height);
    }

    @Override
    public void resize(Dimension d)
    {
        wrappedApplet.resize(d);
    }

    @Override
    public void init()
    {
        if (wrappedApplet != null)
        {
            wrappedApplet.init();
        }
    }

    @Override
    public void start()
    {
        wrappedApplet.start();
        active = true;
    }

    @Override
    public void stop()
    {
        wrappedApplet.stop();
        active = false;
    }

    public void destroy()
    {
        wrappedApplet.destroy();
    }

    @Override
    public URL getCodeBase() {
        try {
            return new URL("http://www.minecraft.net/game/");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public URL getDocumentBase()
    {
        try {
            // Special case only for Classic versions
            if (wrappedApplet.getClass().getCanonicalName().startsWith("com.mojang")) {
                return new URL("http", "www.minecraft.net", 80, "/game/", null);
            }
            return new URL("http://www.minecraft.net/game/");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void setVisible(boolean b)
    {
        super.setVisible(b);
        wrappedApplet.setVisible(b);
    }
    public void update(Graphics paramGraphics)
    {
    }
    public void paint(Graphics paramGraphics)
    {
    }
}
