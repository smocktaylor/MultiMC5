/*
 * Copyright 2012-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

package org.multimc;

public class ParseException extends java.lang.Exception
{
    public ParseException() { super(); }
    public ParseException(String message) {
        super(message);
    }
}
