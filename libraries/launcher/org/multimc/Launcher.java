/*
 * Copyright 2012-2022 MultiMC Contributors
 *
 * This source is subject to the Microsoft Permissive License (MS-PL).
 * Please see the COPYING.md file for more information.
 */

package org.multimc;

public interface Launcher
{
    abstract int launch(ParamBucket params);
}
